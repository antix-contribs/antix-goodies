# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-22 20:45+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

#: android-device-usb-connect.sh:12
msgid "Android Device USB Connect"
msgstr "Android Device USB Connect"

#: android-device-usb-connect.sh:18
msgid "Clearing files and preparing directories"
msgstr "Clearing files and preparing directories"

#: android-device-usb-connect.sh:27
msgid "Checking availability of required utilities"
msgstr "Checking availability of required utilities"

#: android-device-usb-connect.sh:31
msgid "Error: yad is not available"
msgstr "Error: yad is not available"

#: android-device-usb-connect.sh:35
msgid "Error: jmtpfs is not available"
msgstr "Error: jmtpfs is not available"

#: android-device-usb-connect.sh:37
msgid "Checking availability of required utilities finished successfully"
msgstr "Checking availability of required utilities finished successfully"

#: android-device-usb-connect.sh:43
msgid "An android device seems to be mounted"
msgstr "An android device seems to be mounted"

#: android-device-usb-connect.sh:44
msgid ""
"An android device seems to be mounted.\\n \\nChoose 'Unmount' to unplug it "
"safely OR \\n Choose 'Access device' to view the device's contents again.   "
msgstr ""
"An android device seems to be mounted.\\n \\nChoose 'Unmount' to unplug it "
"safely OR \\n Choose 'Access device' to view the device's contents again.   "

#: android-device-usb-connect.sh:44
msgid "Access device"
msgstr "Access device"

#: android-device-usb-connect.sh:44
msgid "Unmount"
msgstr "Unmount"

#: android-device-usb-connect.sh:46
msgid "User has chosen to access the android device"
msgstr "User has chosen to access the android device"

#: android-device-usb-connect.sh:47
msgid "User has chosen to unmount the android device"
msgstr "User has chosen to unmount the android device"

#: android-device-usb-connect.sh:50 android-device-usb-connect.sh:51
msgid "Android device WAS NOT umounted for some reason, do not unplug!"
msgstr "Android device WAS NOT umounted for some reason, do not unplug!"

#: android-device-usb-connect.sh:51 android-device-usb-connect.sh:54
msgid "OK"
msgstr "OK"

#: android-device-usb-connect.sh:53 android-device-usb-connect.sh:54
msgid "Android device is umounted; it is safe to unplug!"
msgstr "Android device is umounted; it is safe to unplug!"

#: android-device-usb-connect.sh:66
msgid "No device connected"
msgstr "No device connected"

#: android-device-usb-connect.sh:67
msgid "Device is connected"
msgstr "Device is connected"

#: android-device-usb-connect.sh:69
msgid ""
"No (MTP enabled) Android device found!\\n  \\n Connect a single device using"
" its USB cable and \\n make sure to select 'MTP' or 'File share' option and "
"retry.   \\n"
msgstr ""
"No (MTP enabled) Android device found!\\n  \\n Connect a single device using"
" its USB cable and \\n make sure to select 'MTP' or 'File share' option and "
"retry.   \\n"

#: android-device-usb-connect.sh:69
msgid "EXIT"
msgstr "EXIT"

#: android-device-usb-connect.sh:69
msgid "Retry"
msgstr "Retry"

#: android-device-usb-connect.sh:71
msgid "User pressed Exit"
msgstr "User pressed Exit"

#: android-device-usb-connect.sh:72
msgid "User pressed Retry"
msgstr "User pressed Retry"

#: android-device-usb-connect.sh:82
msgid "Device is mounted!"
msgstr "Device is mounted!"

#: android-device-usb-connect.sh:84
msgid "Device is NOT mounted!"
msgstr "Device is NOT mounted!"

#: android-device-usb-connect.sh:86
msgid "Attempted to mount device and display its contents"
msgstr "Attempted to mount device and display its contents"

#: android-device-usb-connect.sh:91
msgid ""
"Checking if device can be mounted, asking user to grant permission on the "
"device and try to mount again"
msgstr ""
"Checking if device can be mounted, asking user to grant permission on the "
"device and try to mount again"

#: android-device-usb-connect.sh:93
msgid "Device seems properly mounted!"
msgstr "Device seems properly mounted!"

#: android-device-usb-connect.sh:95 android-device-usb-connect.sh:100
msgid ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed"
msgstr ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed"

#: android-device-usb-connect.sh:95
msgid ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed\\n \\n Note: If you did not allow access, simply "
"unplug, allow permission, and plug in your device's USB cable once more"
msgstr ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed\\n \\n Note: If you did not allow access, simply "
"unplug, allow permission, and plug in your device's USB cable once more"

#: android-device-usb-connect.sh:98
msgid ""
"Final check to see if device can be mounted. If not, unmount it to avoid any"
" errors"
msgstr ""
"Final check to see if device can be mounted. If not, unmount it to avoid any"
" errors"

#: android-device-usb-connect.sh:99
msgid "The device seems to be correctly mounted."
msgstr "The device seems to be correctly mounted."

#: android-device-usb-connect.sh:100
msgid ""
" Unable to mount device! \\n Please check you correctly selected 'MTP...' or"
" 'File transfer...' option.\\n Or 'Allowed' file access.\\n \\n Unplug, "
"allow permission, and plug in your device and try again."
msgstr ""
" Unable to mount device! \\n Please check you correctly selected 'MTP...' or"
" 'File transfer...' option.\\n Or 'Allowed' file access.\\n \\n Unplug, "
"allow permission, and plug in your device and try again."
