# Translations for unplugdrive.sh package (Ver.0.93).
# Copyright (C) 2021 THE unplugdrive'S COPYRIGHT HOLDER
# This file is distributed under the same license as the unplugdrive package.
# Automatically generated, 2021.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: unplugdrive.sh 0.93\n"
"Report-Msgid-Bugs-To: forum.antiXlinux.com\n"
"POT-Creation-Date: 2021-10-26 19:30+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: antiX-contribs (transifex.com)\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. !!! Please use “raw” editor mode and enable “Display spaces” in transifex editor settings in order to see all relevant pieces of information for all entries in this resource. !!!
#. This gets displayed in system tray and main dialog window frame.
#: unplugdrive.sh:47
msgid "Unplug USB device"
msgstr ""

#. This will be follewed by the version number. Please take care to preserve the trailing blank.
#: unplugdrive.sh:49
msgid "Version "
msgstr ""

#. This refers to multiple strings saying ”<device> on <mountpoint>” in a list.
#: unplugdrive.sh:84
msgid "on"
msgstr ""

#. This string will get displayed in main dialog if a drive is greyed out due to
#. the fact that an active nested mount was found on it, so it can't get unmounted.
#. Since it is displayed in a list, not much space for elegible explanations. Please
#. try to translate this as short as the original phrase.
#: unplugdrive.sh:85
msgid "Blocked by nested mount"
msgstr ""

#. Header of the main dialog window. Will be formated in bold big letters.
#: unplugdrive.sh:86
msgid "Mounted USB Partitions"
msgstr ""

#. Text of main dialog window. It will appear above the list of unpluggable devices.
#: unplugdrive.sh:87
msgid "Choose the drive(s) to be unplugged:"
msgstr ""

#. Footnote in main dialog, visible if encrypted devices are found.
#: unplugdrive.sh:88
msgid "Encrypted devices — will get closed if selected for unplugging."
msgstr ""

#. This string is part of the list structure, which runs e.g. as follows:
#. Device sdb1 (Blocked by nested mount)
#. Make sure to translate to a single short word.
#: unplugdrive.sh:88
msgid "Device"
msgstr ""

#. button engraving
#: unplugdrive.sh:89
msgid "Abort"
msgstr ""

#. button engraving
#: unplugdrive.sh:90
msgid "Proceed"
msgstr ""

#. Please take care of the two preceding blank characters. Name can get exchanged for facelifting. Position: help message on console.
#: unplugdrive.sh:101
msgid "  Unplugdrive"
msgstr ""

#. Please take care of the two preceding blank characters. Position: Help message on console. Max length per line: 80 characters. If translation takes more than one line use line breaks of type “\n”.
msgid "  GUI tool for safely unplugging removable USB devices."
msgstr ""

#. Please take care of the two preceding blank characters. Position: Help message on console. Merely the part “unplugdrive.sh” of the line is untranslatable since it refers to start command of the program.
#: unplugdrive.sh:105
msgid "  Usage: unplugdrive.sh [options]"
msgstr ""

#. Please take care of the two preceding blank characters. Position: Help message on console.
#: unplugdrive.sh:107
msgid "  Options:"
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:108
msgid "\\t-h or --help\\t\\tdisplays this help text"
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:109
msgid ""
"\\t-u or --unsafe\\t\\truns script in unsafe mode,\\n\\t            \\t"
"\\tomiting some checks and dialogs"
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:110
msgid ""
"\\t-i or --ignore\\t\\tignores whether devices are\\n\\t            \\t"
"\\treported as rotational even in\\n\\t            \\t\\tdefault mode. No "
"spindown."
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:167
msgid ""
"\\t-x or --exclude\\t\\texclude sd-cards and mmc-devices\\n\\t            \\t"
"\\tfrom detection"
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:111
msgid "\\t-d or --decorated\\tuses window decorations"
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:112
msgid "\\t-c or --centred\\t\\topen dialogs centred"
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:170
msgid "\\t-s or --scrollbars\\tuse scrollbars in main dialog window"
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:171
msgid ""
"\\t-f or --flat\\t\\tdon't draw separator lines in main\\n\\t            \\t"
"\\tdialog window for a flat design"
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:113
msgid "\\t-g or --debug\\t\\tenable debug output (terminal)"
msgstr ""

#. Position: Help message on console. The backslash escape “\t” preceeding words means horizontal tab, following words are meant to be translated. Please check all entries to line up in coulumns with all other entries correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if needed. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please use one or more additional “\n” as linebreaks and make sure to add preceeding horizontal tabs and blanks to line up continuation with the correct column.
#: unplugdrive.sh:114
msgid ""
"\\t-p or --pretend\\t\\tdry run, don't actually un-\\n\\t            \\t"
"\\tmount drives (for debugging)"
msgstr ""

#. Please take care of the two preceding blank characters. Position: Help message on console. Characters preceded by single slash “-” are untranslatable.
#: unplugdrive.sh:116
msgid "  NEVER use the options -u and -i on rotational devices!"
msgstr ""

#. Please take care of the two preceding blank characters, to be repeated at start of each new line. Position: Help message on console. 
#: unplugdrive.sh:177
msgid ""
"  If unplugdrive fails on your device you may call it using\\n  SUDO, which "
"will utilise different methods while processing."
msgstr ""

#. Please take care of the two preceding blank characters. Position: Help message on console.
#: unplugdrive.sh:118
msgid "  Questions, Suggestions and Bugreporting please to:"
msgstr ""

#. Position: Error message on console. Words preceeded by double slash “--” are untranslatable, as well as single characters preceded by single slash “-”.
#. The backslash escape “\n” means new line, words preceeded by this are translatable.
#: unplugdrive.sh:151
msgid ""
"Invalid command line argument. Please call\\nunplugdrive -h or --help for "
"usage instructions."
msgstr ""

#. Position: Error message on console. The backslash escape “\n” means new line. Default double Quotes are not allowed, please use replacement chracters like e.g. „“”»«
#: unplugdrive.sh:167
msgid ""
"\\nˮyadˮ is not installed.\\n   --> Please install ˮyadˮ before executing "
"this script.\\n"
msgstr ""

#. Position: Error message on console. The placeholder “$icon_taskbar” is untranslatable. Max 80 characters per line, add lines with backslash escape ”\n” if needed.
#: unplugdrive.sh:168
msgid "Taskbar icon $icon_taskbar not found."
msgstr ""

#. Position: Info Dialog Window. Please take care to preseve bold formatting “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the arrangement of complete info window in translation. You may use additional lines.
#: unplugdrive.sh:186
msgid "<b>Aborting</b> on user request\\n<b>without unmounting.</b>"
msgstr ""

#. Header of error dialog, will printed in big bold letters.
#: unplugdrive.sh:200
msgid "Closing Encryption failed."
msgstr ""

#. Part of text of error dialog, normal size, in red
#: unplugdrive.sh:200
msgid "The following encrypted partition(s) could not get closed:"
msgstr ""

#. Part of text of error dialog, normal size, in bold letters.
#: unplugdrive.sh:200
msgid "Please make sure these are closed before proceeding!"
msgstr ""

#. Part of text of error dialog,
#: unplugdrive.sh:200
msgid "If you can't close an encrypted device, please abort unmounting."
msgstr ""

#. Button engraving
#: unplugdrive.sh:201
msgid "Abort unmounting"
msgstr ""

#. Button engraving
#: unplugdrive.sh:201
msgid "I have closed them now. Proceed"
msgstr ""

#. Position: Info Dialog Window. Please take care to preseve bold formatting “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the arrangement of complete info window in translation. You may use additional lines by using backslash escape “\n”.
#: unplugdrive.sh:246
msgid ""
"A removable drive with a mounted\\npartition was not found.\\n<b>It is safe "
"to unplug the drive(s)</b>"
msgstr ""

#. Position: Info on command line.
#: unplugdrive.sh:465
msgid "Please wait until encrypted file systems are closed..."
msgstr ""

#. Position: Info Dialog Window.
#: unplugdrive.sh:469
msgid "Encrypted partitions\\nare closed. <b>Please wait...</b>"
msgstr ""

#. Position: Info Dialog Window. Please take care to preseve bold formatting “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the arrangement of complete info window in translation. You may use additional lines.
#: unplugdrive.sh:436
msgid "Data is being written\\nto devices. <b>Please wait...</b>"
msgstr ""

#. Position: Warning message on console window. Please preserve ANSI escape sequences “\\033[1;31m” “\\033[0m” and “\\033[4;37m” and backslash escapes “\n” for formatting.
#. The placeholder “${deviceslist[u]}” is untranslatable. You may redesign the complete message if you need more words or lines for translation. Max 80 characters per line.
#: unplugdrive.sh:526
msgid ""
"\\033[1;31mWARNING: DEVICE ${mnt_devices_paths[$v]} WAS NOT PROPERLY "
"UNMOUNTED!\\033[0m\\n\\033[4;37mPlease check before unplugging.\\033[0m"
msgstr ""

#. Position: Message on console window. Max 80 characters per line, you may use backslash escape “\n” for newline if needed.
#: unplugdrive.sh:561
msgid "Please wait until device(s) spun down..."
msgstr ""

#. Position: Info Dialog Window. Please take care to preseve bold formatting “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the arrangement of complete info window in translation. You may use additional lines.
#: unplugdrive.sh:565
msgid "Rotational devices\\nspin down. <b>Please wait...</b>"
msgstr ""

#. Position: Info Dialog Window. Please take care to preseve bold formatting “<b>...</b>” and backslash escapes “\n” and “\t” meaning newline and horizontal tab.
#. Placeholders “$summarylist” and “$txt_rotationalerror” are untranslatable.
#. You may redesign complete info dialog if translation needs this. You may check and adjust formatting of dialog after this using e.g. aCMSTS.
#: unplugdrive.sh:609
msgid ""
"<big><b>Unmounted:</b></big>\\n$summarylist\\n\\nBut despite the following "
"devices were reported to be rotational\\nthey <span foreground='red'>did not "
"respond to spindown command</span>:\\n\\t$txt_rotationalerror\\nPlease check "
"whether they are spun down before unplugging.\\n<b>After rotational drives "
"are spun down it is safe to unplug.</b>"
msgstr ""

#. Position: Info Dialog Window. Please take care to preseve bold formatting “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the arrangement of complete info window in translation. You may use additional lines.
#: unplugdrive.sh:613
msgid ""
"<big><b>Unmounted:</b></big>\\n$summarylist\\n\\n<b>It is safe to unplug the "
"drive(s)</b>"
msgstr ""

#. Position: Part of Info Dialog Window. Please take care to preseve bold formatting “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the arrangement of complete info window in translation. You may use additional lines.
#: unplugdrive.sh:623
msgid ""
"<b><big>Mountpoint removal failed.</big></b>\\n<span "
"foreground='red'><u><b>One or more mountpoin(s) remain present at:</b></u></"
"span>"
msgstr ""

#. Position: Part of Info Dialog Window. Please take care to preseve bold formatting “<b>...</b>”.
#. If translation takes more characters or words, you may want to redesign the arrangement of complete info window in translation. You may use additional lines.
#: unplugdrive.sh:623
msgid "<b>Check each mountpoint listed before unpluging the drive(s).</b>"
msgstr ""
