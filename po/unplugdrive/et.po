# Translations for unplugdrive.sh package (Ver.0.93).
# Copyright (C) 2021 THE unplugdrive'S COPYRIGHT HOLDER
# This file is distributed under the same license as the unplugdrive package.
# Automatically generated, 2021.
# 
# Translators:
# Robin, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: unplugdrive.sh 0.93\n"
"Report-Msgid-Bugs-To: forum.antiXlinux.com\n"
"POT-Creation-Date: 2021-10-26 19:30+0200\n"
"PO-Revision-Date: 2021-06-01 11:01+0000\n"
"Last-Translator: Robin, 2021\n"
"Language-Team: Estonian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/et/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: et\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. !!! Please use “raw” editor mode and enable “Display spaces” in transifex
#. editor settings in order to see all relevant pieces of information for all
#. entries in this resource. !!!
#. This gets displayed in system tray and main dialog window frame.
#: unplugdrive.sh:47
msgid "Unplug USB device"
msgstr "Ühendage USB-seade lahti"

#. This will be follewed by the version number. Please take care to preserve
#. the trailing blank.
#: unplugdrive.sh:49
msgid "Version "
msgstr "Versioon "

#. This refers to multiple strings saying ”<device> on <mountpoint>” in a
#. list.
#: unplugdrive.sh:84
msgid "on"
msgstr "peal"

#. This string will get displayed in main dialog if a drive is greyed out due
#. to
#. the fact that an active nested mount was found on it, so it can't get
#. unmounted.
#. Since it is displayed in a list, not much space for elegible explanations.
#. Please
#. try to translate this as short as the original phrase.
#: unplugdrive.sh:85
msgid "Blocked by nested mount"
msgstr "Blokeeritud pesastatud kinnitusega"

#. Header of the main dialog window. Will be formated in bold big letters.
#: unplugdrive.sh:86
msgid "Mounted USB Partitions"
msgstr "Paigaldatud USB -vaheseinad"

#. Text of main dialog window. It will appear above the list of unpluggable
#. devices.
#: unplugdrive.sh:87
msgid "Choose the drive(s) to be unplugged:"
msgstr "Valige draiv(id), mille vooluvõrgust lahti ühendada:"

#. Footnote in main dialog, visible if encrypted devices are found.
#: unplugdrive.sh:88
msgid "Encrypted devices — will get closed if selected for unplugging."
msgstr "Krüptitud seadmed – suletakse, kui need valitakse lahtiühendamiseks."

#. This string is part of the list structure, which runs e.g. as follows:
#. Device sdb1 (Blocked by nested mount)
#. Make sure to translate to a single short word.
#: unplugdrive.sh:88
msgid "Device"
msgstr "Seade"

#. button engraving
#: unplugdrive.sh:89
msgid "Abort"
msgstr "Katkesta"

#. button engraving
#: unplugdrive.sh:90
msgid "Proceed"
msgstr "Jätkake"

#. Please take care of the two preceding blank characters. Name can get
#. exchanged for facelifting. Position: help message on console.
#: unplugdrive.sh:101
msgid "  Unplugdrive"
msgstr "  Eemaldage pistik"

#. Please take care of the two preceding blank characters. Position: Help
#. message on console. Max length per line: 80 characters. If translation
#. takes more than one line use line breaks of type “\n”.
msgid "  GUI tool for safely unplugging removable USB devices."
msgstr "  GUI-tööriist eemaldatavate USB-seadmete ohutuks lahtiühendamiseks."

#. Please take care of the two preceding blank characters. Position: Help
#. message on console. Merely the part “unplugdrive.sh” of the line is
#. untranslatable since it refers to start command of the program.
#: unplugdrive.sh:105
msgid "  Usage: unplugdrive.sh [options]"
msgstr "  Kasutamine: unplugdrive.sh [valikuid]"

#. Please take care of the two preceding blank characters. Position: Help
#. message on console.
#: unplugdrive.sh:107
msgid "  Options:"
msgstr "  Valikud:"

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:108
msgid "\\t-h or --help\\t\\tdisplays this help text"
msgstr "\\t-h või --help\\t\\tkuvab selle abiteksti"

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:109
msgid ""
"\\t-u or --unsafe\\t\\truns script in unsafe mode,\\n\\t            "
"\\t\\tomiting some checks and dialogs"
msgstr ""
"\\t-u või --unsafe\\t\\tkäivitab skripti ebaturvalises režiimis,\\n\\t"
"            \\t\\tjättes vahele mõned kontrollid ja dialoogi"

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:110
msgid ""
"\\t-i or --ignore\\t\\tignores whether devices are\\n\\t            "
"\\t\\treported as rotational even in\\n\\t            \\t\\tdefault mode. No"
" spindown."
msgstr ""
"\\t-i või --ignore\\t\\tignoreerib, kas seadmed on\\n\\t            \\t\\ton"
" teatatud rotatsioonina isegi aastal\\n\\t            \\t\\tvaikerežiim. Ei "
"mingit spindowni."

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:167
msgid ""
"\\t-x or --exclude\\t\\texclude sd-cards and mmc-devices\\n\\t            "
"\\t\\tfrom detection"
msgstr ""
"\\t-x või --exclude\\t\\tvälistada sd-kaardid ja mmc-seadmed\\n\\t"
"            \\t\\ttuvastamisest"

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:111
msgid "\\t-d or --decorated\\tuses window decorations"
msgstr "\\t-d või --decorated\\tkasutab aknakaunistusi"

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:112
msgid "\\t-c or --centred\\t\\topen dialogs centred"
msgstr "\\t-c või --centred\\t\\tavatud dialoogid keskel"

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:170
msgid "\\t-s or --scrollbars\\tuse scrollbars in main dialog window"
msgstr "\\t-s või --scrollbars\\tkasutage dialoogi põhiaknas kerimisribasid"

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:171
msgid ""
"\\t-f or --flat\\t\\tdon't draw separator lines in main\\n\\t            "
"\\t\\tdialog window for a flat design"
msgstr ""
"\\t-f või --flat\\t\\tärge tõmmake peamisse eraldusjooni\\n\\t            "
"\\t\\tlameda kujunduse dialoogiaken"

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:113
msgid "\\t-g or --debug\\t\\tenable debug output (terminal)"
msgstr "\\t-g või --debug\\t\\tluba silumisväljund (terminal)"

#. Position: Help message on console. The backslash escape “\t” preceeding
#. words means horizontal tab, following words are meant to be translated.
#. Please check all entries to line up in coulumns with all other entries
#. correctly. Adjust by adding or removing blanks and horizontal tabs ”\t” if
#. needed. Words preceeded by double slash “--” are untranslatable, as well as
#. single characters preceded by single slash “-”.
#. Max 80 characters per line. If translation takes more characters, please
#. use one or more additional “\n” as linebreaks and make sure to add
#. preceeding horizontal tabs and blanks to line up continuation with the
#. correct column.
#: unplugdrive.sh:114
msgid ""
"\\t-p or --pretend\\t\\tdry run, don't actually un-\\n\\t            "
"\\t\\tmount drives (for debugging)"
msgstr ""
"\\t-p või --pretend\\t\\tkuivjooks, ärge tegelikult loobuge\\n\\t"
"            \\t\\tühendage draivid (silumiseks)"

#. Please take care of the two preceding blank characters. Position: Help
#. message on console. Characters preceded by single slash “-” are
#. untranslatable.
#: unplugdrive.sh:116
msgid "  NEVER use the options -u and -i on rotational devices!"
msgstr "  ÄRGE kasutage pöörlevatel seadmetel KUNAGI suvandeid -u ja -i!"

#. Please take care of the two preceding blank characters, to be repeated at
#. start of each new line. Position: Help message on console.
#: unplugdrive.sh:177
msgid ""
"  If unplugdrive fails on your device you may call it using\\n  SUDO, which "
"will utilise different methods while processing."
msgstr ""
"  Kui seadme lahtiühendamine ebaõnnestub, võite sellele helistada\\n  SUDO, "
"mis kasutab töötlemise ajal erinevaid meetodeid."

#. Please take care of the two preceding blank characters. Position: Help
#. message on console.
#: unplugdrive.sh:118
msgid "  Questions, Suggestions and Bugreporting please to:"
msgstr "  Küsimused, soovitused ja veateated palun:"

#. Position: Error message on console. Words preceeded by double slash “--”
#. are untranslatable, as well as single characters preceded by single slash
#. “-”.
#. The backslash escape “\n” means new line, words preceeded by this are
#. translatable.
#: unplugdrive.sh:151
msgid ""
"Invalid command line argument. Please call\\nunplugdrive -h or --help for "
"usage instructions."
msgstr ""
"Kehtetu käsurea argument. Palun helistage\\nunplugdrive -h või --help "
"kasutusjuhiste jaoks."

#. Position: Error message on console. The backslash escape “\n” means new
#. line. Default double Quotes are not allowed, please use replacement
#. chracters like e.g. „“”»«
#: unplugdrive.sh:167
msgid ""
"\\nˮyadˮ is not installed.\\n   --> Please install ˮyadˮ before executing "
"this script.\\n"
msgstr ""
"\\nˮyadˮ pole installitud.\\n   --> Palun installige ˮyadˮ enne selle "
"skripti käivitamist.\\n"

#. Position: Error message on console. The placeholder “$icon_taskbar” is
#. untranslatable. Max 80 characters per line, add lines with backslash escape
#. ”\n” if needed.
#: unplugdrive.sh:168
msgid "Taskbar icon $icon_taskbar not found."
msgstr "Tegumiriba ikoon $icon_taskbar ei leitud."

#. Position: Info Dialog Window. Please take care to preseve bold formatting
#. “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the
#. arrangement of complete info window in translation. You may use additional
#. lines.
#: unplugdrive.sh:186
msgid "<b>Aborting</b> on user request\\n<b>without unmounting.</b>"
msgstr "<b>Katkestamine</b> kasutaja soovil\\n<b>lahtimonteerimata.</b>"

#. Header of error dialog, will printed in big bold letters.
#: unplugdrive.sh:200
msgid "Closing Encryption failed."
msgstr "Krüptimise sulgemine ebaõnnestus."

#. Part of text of error dialog, normal size, in red
#: unplugdrive.sh:200
msgid "The following encrypted partition(s) could not get closed:"
msgstr "Järgmisi krüptitud partitsiooni(sid) ei õnnestunud sulgeda:"

#. Part of text of error dialog, normal size, in bold letters.
#: unplugdrive.sh:200
msgid "Please make sure these are closed before proceeding!"
msgstr "Enne jätkamist veenduge, et need on suletud!"

#. Part of text of error dialog,
#: unplugdrive.sh:200
msgid "If you can't close an encrypted device, please abort unmounting."
msgstr "Kui te ei saa krüptitud seadet sulgeda, katkestage lahtiühendamine."

#. Button engraving
#: unplugdrive.sh:201
msgid "Abort unmounting"
msgstr "Katkestage lahtiühendamine"

#. Button engraving
#: unplugdrive.sh:201
msgid "I have closed them now. Proceed"
msgstr "Nüüd olen need sulgenud. Jätkake"

#. Position: Info Dialog Window. Please take care to preseve bold formatting
#. “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the
#. arrangement of complete info window in translation. You may use additional
#. lines by using backslash escape “\n”.
#: unplugdrive.sh:246
msgid ""
"A removable drive with a mounted\\npartition was not found.\\n<b>It is safe "
"to unplug the drive(s)</b>"
msgstr ""
"Paigaldatud eemaldatav ajam\\npartitsiooni ei leitud.\\n<b>Ajami(te) "
"vooluvõrgust eemaldamine on ohutu</b>"

#. Position: Info on command line.
#: unplugdrive.sh:465
msgid "Please wait until encrypted file systems are closed..."
msgstr "Palun oodake, kuni krüptitud failisüsteemid suletakse..."

#. Position: Info Dialog Window.
#: unplugdrive.sh:469
msgid "Encrypted partitions\\nare closed. <b>Please wait...</b>"
msgstr "Krüptitud vaheseinad\\non suletud. <b>Palun oota...</b>"

#. Position: Info Dialog Window. Please take care to preseve bold formatting
#. “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the
#. arrangement of complete info window in translation. You may use additional
#. lines.
#: unplugdrive.sh:436
msgid "Data is being written\\nto devices. <b>Please wait...</b>"
msgstr "Andmeid kirjutatakse\\nseadmetele. <b>Palun oota...</b>"

#. Position: Warning message on console window. Please preserve ANSI escape
#. sequences “\\033[1;31m” “\\033[0m” and “\\033[4;37m” and backslash escapes
#. “\n” for formatting.
#. The placeholder “${deviceslist[u]}” is untranslatable. You may redesign the
#. complete message if you need more words or lines for translation. Max 80
#. characters per line.
#: unplugdrive.sh:526
msgid ""
"\\033[1;31mWARNING: DEVICE ${mnt_devices_paths[$v]} WAS NOT PROPERLY "
"UNMOUNTED!\\033[0m\\n\\033[4;37mPlease check before unplugging.\\033[0m"
msgstr ""
"\\033[1;31mHOIATUS: SEADE ${mnt_devices_paths[$v]} EI OLNUD KORRALIKULT "
"PAIGALDATUD!\\033[0m\\n\\033[4;37mPalun kontrollige enne vooluvõrgust "
"eemaldamist.\\033[0m"

#. Position: Message on console window. Max 80 characters per line, you may
#. use backslash escape “\n” for newline if needed.
#: unplugdrive.sh:561
msgid "Please wait until device(s) spun down..."
msgstr "Oodake, kuni seade (seadmed) välja lülitatakse ..."

#. Position: Info Dialog Window. Please take care to preseve bold formatting
#. “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the
#. arrangement of complete info window in translation. You may use additional
#. lines.
#: unplugdrive.sh:565
msgid "Rotational devices\\nspin down. <b>Please wait...</b>"
msgstr "Pöörlemisseadmed\\nkeeruta alla. <b>Palun oota...</b>"

#. Position: Info Dialog Window. Please take care to preseve bold formatting
#. “<b>...</b>” and backslash escapes “\n” and “\t” meaning newline and
#. horizontal tab.
#. Placeholders “$summarylist” and “$txt_rotationalerror” are untranslatable.
#. You may redesign complete info dialog if translation needs this. You may
#. check and adjust formatting of dialog after this using e.g. aCMSTS.
#: unplugdrive.sh:609
msgid ""
"<big><b>Unmounted:</b></big>\\n$summarylist\\n\\nBut despite the following "
"devices were reported to be rotational\\nthey <span foreground='red'>did not"
" respond to spindown command</span>:\\n\\t$txt_rotationalerror\\nPlease "
"check whether they are spun down before unplugging.\\n<b>After rotational "
"drives are spun down it is safe to unplug.</b>"
msgstr ""
"<big><b>Monteerimata:</b></big>\\n$summarylist\\n\\nKuid vaatamata sellele, "
"et järgmised seadmed olid pöörlevad\\nneed <span foreground='red'>ei "
"vastanud spindowni käsule</span>:\\n\\t$txt_rotationalerror\\nEnne "
"vooluvõrgust eemaldamist kontrollige, kas need on maha keeratud.\\n<b>Pärast"
" pöörlevate ajamite mahapööramist on ohutu eemaldada.</b>"

#. Position: Info Dialog Window. Please take care to preseve bold formatting
#. “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the
#. arrangement of complete info window in translation. You may use additional
#. lines.
#: unplugdrive.sh:613
msgid ""
"<big><b>Unmounted:</b></big>\\n$summarylist\\n\\n<b>It is safe to unplug the"
" drive(s)</b>"
msgstr ""
"<big><b>Monteerimata:</b></big>\\n$summarylist\\n\\n<b>Ajami(te) "
"vooluvõrgust eemaldamine on ohutu</b>"

#. Position: Part of Info Dialog Window. Please take care to preseve bold
#. formatting “<b>...</b>” and backslash escape “\n” meaning newline.
#. If translation takes more characters or words, you may want to redesign the
#. arrangement of complete info window in translation. You may use additional
#. lines.
#: unplugdrive.sh:623
msgid ""
"<b><big>Mountpoint removal failed.</big></b>\\n<span "
"foreground='red'><u><b>One or more mountpoin(s) remain present "
"at:</b></u></span>"
msgstr ""
"<b><big>Mountpointi eemaldamine ebaõnnestus.</big></b>\\n<span "
"foreground='red'><u><b>Üks või mitu kinnituspunkti jäävad "
"alles:</b></u></span>"

#. Position: Part of Info Dialog Window. Please take care to preseve bold
#. formatting “<b>...</b>”.
#. If translation takes more characters or words, you may want to redesign the
#. arrangement of complete info window in translation. You may use additional
#. lines.
#: unplugdrive.sh:623
msgid "<b>Check each mountpoint listed before unpluging the drive(s).</b>"
msgstr ""
"<b>Enne draivi(te) lahtiühendamist kontrollige kõiki loetletud "
"kinnituspunkte.</b>"
