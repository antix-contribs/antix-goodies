# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Eduard Selma <selma@tinet.cat>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-27 14:09+0200\n"
"PO-Revision-Date: 2020-02-27 12:11+0000\n"
"Last-Translator: Eduard Selma <selma@tinet.cat>, 2020\n"
"Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: yad-updater.sh:12
msgid "Internet connection detected"
msgstr "Detectada connexió a Internet"

#: yad-updater.sh:18
msgid "already root"
msgstr "Ja sou 'root'"

#: yad-updater.sh:22
msgid "You are Root or running the script in sudo mode"
msgstr "Ja sou 'root' o executeu l'script en mode sudo"

#: yad-updater.sh:24
msgid "You entered the wrong password or you cancelled"
msgstr "Heu entrat una contrasenya incorrecta o heu cancel·lat"

#: yad-updater.sh:31
msgid "Waiting for a Network connection..."
msgstr "Esperant connexió a Internet..."

#: yad-updater.sh:42
msgid "No Internet connection detected!"
msgstr "No s'ha detectat cap connexió a Internet!"
