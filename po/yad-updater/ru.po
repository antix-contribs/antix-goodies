# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Vladimir O <vldoduv@yandex.ru>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-27 14:09+0200\n"
"PO-Revision-Date: 2020-02-27 12:11+0000\n"
"Last-Translator: Vladimir O <vldoduv@yandex.ru>, 2020\n"
"Language-Team: Russian (https://www.transifex.com/anticapitalista/teams/10162/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: yad-updater.sh:12
msgid "Internet connection detected"
msgstr "Обнаружено подключение к Интернету"

#: yad-updater.sh:18
msgid "already root"
msgstr "уже суперпользователь"

#: yad-updater.sh:22
msgid "You are Root or running the script in sudo mode"
msgstr "Вы - суперпользователь или запустили скрипт в sudo режиме"

#: yad-updater.sh:24
msgid "You entered the wrong password or you cancelled"
msgstr "Введён неправильный пароль, или вы выбрали Отмена"

#: yad-updater.sh:31
msgid "Waiting for a Network connection..."
msgstr "Ожидаю Сетевое подключение"

#: yad-updater.sh:42
msgid "No Internet connection detected!"
msgstr "Нет доступа к Интернет!"
