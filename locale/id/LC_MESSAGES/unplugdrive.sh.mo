��    	      d      �       �   [   �   B   =  0   �  �   �  G   ?  .   �  W   �  9     �  H  h   E  I   �  4   �  �   -  M   �  3     M   F  -   �                                          	    A removable drive with a mounted partition was not found.
It is safe to unplug the drive(s) About to unmount:
$summarylist
Please confirm you wish to proceed. Data is being written to devices.
Please wait... Mountpoint removal failed.

A mountpoint remains present at:
$mountpointerrorlist
Check each mountpoint listed before unpluging the drive(s). Nothing has been unmounted.
Aborting as requested with no action taken. Nothing selected.
Aborting without unmounting. The following are currently mounted:
$removablelist
Choose the drive(s) to be unplugged Unmounted:
$summarylist
It is safe to unplug the drive(s) Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-12-15 00:36+0200
PO-Revision-Date: 2019-10-12 15:05+0300
Last-Translator: donny widi <citozpremium@gmail.com>
Language-Team: Indonesian (http://www.transifex.com/anticapitalista/antix-development/language/id/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: id
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.11
X-Launchpad-Export-Date: 2012-01-02 10:46+0000
 Drive yang dapat dilepas dengan partisi yang dipasang tidak ditemukan.
Aman untuk mencabut drive(-drive) Akan dilepas:
$summarylist
Harap konfirmasi bahwa Anda ingin melanjutkan. Data sedang ditulis ke perangkat.
Tunggu sebentar... Penghapusan mountpoint gagal.

Mountpoint tetap ada di:
$mountpointerrorlist
Periksa setiap titik pemasangan yang terdaftar sebelum mencabut drive(s). Tidak ada yang dilepas.
Batalkan seperti yang diminta tanpa tindakan apa pun. Tidak ada yang dipilih.
Pembatalan tanpa pelepasan. Berikut ini sedang dipasang:
$removablelist
Pilih drive(s)
 yang akan dicabut Lepas:
$summarylist
Aman untuk mencabut drive 