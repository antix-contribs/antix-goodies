��          �      �       0     1     B  �   T  G  �  B     $   a  3   �     �     �  F   �     $     5  �  =     �     �  �   �  �  �  L   +
  +   x
  B   �
     �
     �
  C        P  
   h                                
                      	    ADD selected app App .desktop file Choose (or drag and drop to the field below) the .desktop file you want to add to the personal menu \n OR select any other option FPM has no 'graphical' way to allow users to move icons around or delete arbitrary icons.\nIf you click OK, the personal menu configuration file will be opened for editing.\nEach menu icon is identified by a line starting with 'prog' followed by the application name, icon location and the application executable file.\nMove or delete the entire line refering to each personal menu entry.\nNote: Lines starting with # are comments only and will be ignored.\nThere can be empty lines.\nSave any changes and then restart IceWM.\nYou can undo the last change from FPMs 'Restore' button. FTM is programmed to always keep 1 line in the personal menu file! Fast Personal Menu Manager for IceWM No changes were made! Please choose an application. ORGANIZE entries REMOVE last entry This will delete the last entry from your personal menu! Are you sure? UNDO last change Warning Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-26 16:23+0000
Last-Translator: Pierluigi Mario <pierluigimariomail@gmail.com>, 2020
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
 AGGIUNGI app selezionata App file .desktop Scegli (o trascina e rilascia nel campo qui sotto) il file .desktop che vuoi aggiungere al menu personale O seleziona qualsiasi altra opzione GRMP non ha un modo 'grafico' per permettere agli utenti di spostare le icone o di cancellarne di arbitrarie.\nSe clicchi OK, il file di configurazione del menu personale verrà aperto per essere editato.\nOgni icona del menu è identificata da una linea che inizia con 'prog' seguita dal nome del'applicazione, posizione dell'icona e il file eseguibile dell'applicazione.\nSposta o elimina l'intera linea che si riferisce a ciascuna voce del menu personale.\nNota: Le linee che iniziano con # sono solo commenti e saranno ignorate.\nPossono esserci linee vuote. Salva qualsiasi modifica e poi riavvia IceWM.\nPuoi annullare l'ultima modifica dal pulsante 'Ripristina' di GRMP. GRMP è programmato per tenere sempre una linea nel file del menu personale! Gestore Rapido del Menu Personale per IceWM Non è stata fatta nessuna modifica! Prego scegli un'applicazione. ORGANIZZA voci RIMUOVI l'ultima voce Questo eliminerà l'ultima voce dal tuo menu personale! Sei sicuro? ANNULLA ultima modifica Attenzione 