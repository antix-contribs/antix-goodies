��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  -     /   L  !   |  +   �  '   �     �               3     <     O     a  #   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-12 19:14+0000
Last-Translator: Wallon Wallon
Language-Team: French (Belgium) (http://www.transifex.com/anticapitalista/antix-development/language/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=2; plural=(n > 1);
 (Connexion au périphérique choisi réussie) (Connexion au périphérique choisi impossible) Carte son par défaut actuelle %s Aucun périphérique ou carte son détecté Une seule carte son a été détectée. Veuillez choisir une carte son Quitter Carte son réglée sur %s Test son Le test a échoué Le test a réussi Test du son pendant 6 secondes Souhaitez-vous tester la carte son? 