��          T      �       �      �       �   #   �   /     /   K     {  �  �           -  "   N  *   q  (   �     �                                        Internet connection detected No Internet connection detected! Waiting for a Network connection... You are Root or running the script in sudo mode You entered the wrong password or you cancelled already root Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-27 12:11+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Fant internett-tilkobling Fant ingen internett-tilkobling! Venter på nettverkstilkobling … Du er root eller kjører skriptet med sudo Du skrev inn feil passord, eller avbrøt allerede root 