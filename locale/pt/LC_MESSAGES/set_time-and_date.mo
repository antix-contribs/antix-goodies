��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    =   �     �       #     %   C     i     n     �     �  R   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-25 22:19+0300
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2021
Language-Team: Portuguese (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Escolher a Zona Horária (usando as teclas do cursor e Enter) Data: Definições de Data e Hora Mover o cursor para a Hora correcta Mover o cursor para o Minuto correcto Sair Seleccionar a Zona Horária Introduzir a Data Actual Introduzir a Hora Actual Usar o servidor de 'Hora da Internet' para definir automaticamente a data e a hora 