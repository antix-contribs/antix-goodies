��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    "   �     �  N   �  I   N  M   �     �  "   �  8     *   O  �   z                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-25 22:19+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: Greek (https://www.transifex.com/antix-linux-community-contributions/teams/120110/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Επιλέξτε Ζώνη ώρας Ημερομηνία: Διαχείριση ρυθμίσεων ημερομηνίας και ώρας Μετακινήστε το ρυθμιστικό στη σωστή ώρα Μετακινήστε το ρυθμιστικό στο σωστό λεπτό Εξοδος Επιλέξτε Ζώνη ώρας Ορισμός τρέχουσας ημερομηνίας Ορισμός τρέχουσας ώρας Χρησιμοποιήστε το διακομιστή ώρας Internet για να ορίσετε αυτόματα ώρα/ημερομηνία 