��          4      L       `   O  a   b   �  �    P  �  e   �                    <big><b>Unmounted:</b></big>\n$summarylist\n\nBut despite the following devices were reported to be rotational\nthey <span foreground='red'>did not respond to spindown command</span>:\n\t$txt_rotationalerror\nPlease check whether they are spun down before unplugging.\n<b>After rotational drives are spun down it is safe to unplug.</b> \t-u or --unsafe\t\truns script in unsafe mode,\n\t            \t\tomiting some checks and dialogs Project-Id-Version: unplugdrive.sh 0.93
Report-Msgid-Bugs-To: forum.antiXlinux.com
PO-Revision-Date: 2021-06-01 11:01+0000
Last-Translator: Robin, 2021
Language-Team: English (https://www.transifex.com/antix-linux-community-contributions/teams/120110/en/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 <big><b>Unmounted:</b></big>\n$summarylist\n\nBut despite the following devices were reported to be rotational\nthese <span foreground='red'>did not respond to spindown command</span>:\n\t$txt_rotationalerror\nPlease check whether they are spun down before unplugging.\n<b>After rotational drives are spun down it is safe to unplug.</b> \t-u or --unsafe\t\truns script in unsafe mode,\n\t            \t\tomitting some checks and dialogues 