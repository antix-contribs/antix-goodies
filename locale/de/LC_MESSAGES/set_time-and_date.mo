��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    5   �     �  *   �  5     1   >     p     x     �      �  @   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-25 22:18+0300
Last-Translator: Robin, 2021
Language-Team: German (https://www.transifex.com/antix-linux-community-contributions/teams/120110/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Wählen Sie die Zeitzone (mit Cursortasten und Enter) Datum: Manager für Datums- und Zeiteinstellungen Bewegen Sie den Schieberegler auf die richtige Stunde Schieberegler auf die richtige Minute verschieben Beenden Wählen Sie die Zeitzone Einstellen des aktuellen Datums Einstellen der aktuellen Uhrzeit Datum und Uhrzeit automatisch von einem Internet-Server beziehen 