��          �      �       0     1     B  �   T  G  �  B     $   a  3   �     �     �  F   �     $     5  �  =     �     �  �   
  �  �  R   |
  %   �
  /   �
     %     8  F   S     �     �                                
                      	    ADD selected app App .desktop file Choose (or drag and drop to the field below) the .desktop file you want to add to the personal menu \n OR select any other option FPM has no 'graphical' way to allow users to move icons around or delete arbitrary icons.\nIf you click OK, the personal menu configuration file will be opened for editing.\nEach menu icon is identified by a line starting with 'prog' followed by the application name, icon location and the application executable file.\nMove or delete the entire line refering to each personal menu entry.\nNote: Lines starting with # are comments only and will be ignored.\nThere can be empty lines.\nSave any changes and then restart IceWM.\nYou can undo the last change from FPMs 'Restore' button. FTM is programmed to always keep 1 line in the personal menu file! Fast Personal Menu Manager for IceWM No changes were made! Please choose an application. ORGANIZE entries REMOVE last entry This will delete the last entry from your personal menu! Are you sure? UNDO last change Warning Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-26 16:23+0000
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>, 2020
Language-Team: Galician (Spain) (https://www.transifex.com/anticapitalista/teams/10162/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 ENGADIR entrada Ficheiro .desktop do aplicativo Premer no campo de selección e escoller o ficheiro .desktop a engadir ao menú persoal \n (premer dáus veces ou arrastrar para o campo) OU usar os botóns para outras opcións Este xestor non incluín unha forma 'gráfica' dos utilizadores moveren ou eliminaren entradas.\nPremendo en Aceptar, o ficheiro de configuración do menú persoal abre para edición.\nCada entrada do menú é identificada no ficheiro por unha liña constituída por 'prog' seguido do nome da aplicación, da localización da icona e do ficheiro executable do aplicativo.\nPara modificar o menú, mover ou eliminar a liña completa relativa a cada entrada do menú.\nNota: As liñas comezadas por # son só comentarios e son ignoradas.\nPode haber liñas vacías.\nGardar calquera cambios feitos e reiniciar o IceWM.\nÉ posible reverter o último cambio a través do botón 'REVERTER o último cambio'. O xestor está programado para manter sempre 1 liña no ficheiro do menú persoal! Xestor do menú persoal, para o IceWM Non se fixeron cambios! Escoller un aplicativo. ORGANIZAR entradas ELIMINAR a última entrada Esta acción eliminará a última entrada do menú persoal. Continuar? REVERTER o último cambio Aviso 